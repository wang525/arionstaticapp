$(function () {
	"use strict";

	/* image tooltip */
	// div has position absolute
	$('.brand, .client').mousemove(function(e){
		var y = e.pageY - $(this).offset().top;
		var x = e.pageX - $(this).offset().left;
		$('.brand-img').css({
			'left': x + 'px',
			'top': y + 'px'
		});
	});

	// div has no position absolute
	$('.contact-img-hover').mousemove(function(e){
		var y = e.pageY;
		var x = e.pageX;
		$('.contact-img').css({
			'left': x + 'px',
			'top': y + 'px'
		});
		console.log("x, y=", e.pageX, e.pageY, this.offsetLeft, this.offsetTop)
	});
	/* END image tooltip */

	/* scroll animation */
	$('.aos-area').scroll(function(e){
		var windowHeight = $(window).height();
		let maxIdx = 0;
		$(".ani-ini").each(function(index) {
			if (($(this).offset().top < 0) || (($(this).offset().top + $(this).height() * 2 /3) < windowHeight)) {
				$(this).addClass("animated"); 
				maxIdx = index;
			} else {
				$(this).removeClass("animated");
				$('.title-ini:nth-child('+ parseInt(index + 1) +')').removeClass('animated');
			}
		});
		$('.title-ini:nth-child('+ parseInt(maxIdx + 1) +')').addClass('animated');
		for (let i = 0; i < maxIdx; i++) {
			$('.title-ini:nth-child('+ parseInt(i + 1) +')').removeClass('animated');
		}
	});

	/* when click image show details image in us tab */
	$('.team-member').click(function (event) {
		if (!$(this).find('.details').hasClass('open')) {
			$(this).find('.details').addClass('open');
			event.stopPropagation();
		}
	});

	$(function() {
		$("body").click(function(e) {
			var container = $(".detail-img img");
			if (!container.is(e.target) && container.has(e.target).length === 0) {
				if ($('.team-lists .details').hasClass('open')) {
					$('.team-lists .details').removeClass('open')
					e.stopPropagation();
				}
			}
		});
	})

	/* scroll animation for core-values */
	var curIdx = 1;
	// $(function() {
	// 	setInterval(showActiveValue, 4000)
	// })
	function showActiveValue() {
		$('[id^=auto_load_]').removeClass('active');
		$('#auto_load_'+curIdx).toggleClass('active');
		curIdx++;
		curIdx = (curIdx > 5) ? 1 : curIdx; 
	}
	
	
});