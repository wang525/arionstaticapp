(function($, window, document, undefined) {
    'use strict';

		$('#demoprogressbar5').LineProgressbar({
			percentage: 25,
			fillBackgroundColor: '#1abc9c'
			});
			$('#demoprogressbar6').LineProgressbar({
			percentage: 50,
			fillBackgroundColor: '#9b59b6'
			});
			$('#demoprogressbar7').LineProgressbar({
			percentage: 70,
			fillBackgroundColor: '#e67e22'
			});
			$('#demoprogressbar8').LineProgressbar({
			percentage: 95,
			fillBackgroundColor: '#f1c40f'
			});

})(jQuery, window, document);